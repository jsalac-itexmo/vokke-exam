<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KangarooController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[KangarooController::class, "index"] );

Route::as('web.kangaroos.')
->controller(KangarooController::class)
->group(function () {
    Route::get("kangaroos", "index")->name("list");
    Route::get("kangaroos/create", "create")->name("create");
    Route::get("kangaroos/{id}", "edit")->name("edit");
    Route::get("kangaroos/create", "create")->name("edit");
});