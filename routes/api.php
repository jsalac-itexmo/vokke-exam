<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KangarooController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::as('api.kangaroos.')
->controller(KangarooController::class)
->group(function () {
    Route::get("kangaroos", "all")->name("list");
    Route::post("kangaroos", "store")->name("store");
    Route::get("kangaroos/{id}", "get")->name("update");
    Route::patch("kangaroos/{id}", "update")->name("update");
    Route::delete("kangaroos/{id}", "destroy")->name("destroy");
});