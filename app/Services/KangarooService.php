<?php

namespace App\Services;

use App\Traits\Crud;
use App\Models\Kangaroo;

class KangarooService
{
    // methods for CRUD are located in the Crud traits
    use Crud;

    protected $model;
    protected $modelResource;
    protected $fileStoragePath;

    public function __construct(Kangaroo $model)
    {
        $this->model = $model;
        $this->modelResource = "App\Http\Resources\KangarooResource";
        $this->fileStoragePath = "public/Kangaroo";
    }
}