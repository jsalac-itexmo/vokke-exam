<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\KangarooService;
use Illuminate\Support\Facades\Log;
use App\Http\Requests\KangarooRequest;

class KangarooController extends Controller
{

    protected $service;

    /**
     * @param KangarooService
     */
    public function __construct(KangarooService $service)
    {
        $this->service = $service;
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request)
    {
        $kangaroos = $this->service->getAll($request);
        return response()->json(["data" => $kangaroos], 200);
    }

    /**
     * @param  \Illuminate\Http\Request  $request
     * @param $id integer
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request, $id)
    {
        $kangaroo = $this->service->find($id);
        return response()->json(["data" => $kangaroo], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $kangaroos = $this->service->getAll($request);
        return view("kangaroo.index", ["list" => $kangaroos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KangarooRequest $request)
    {
        $response = $this->service->store($request);
        return response()->json($response, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("kangaroo.create");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kangaroo = $this->service->find($id);
        Log::info(json_encode($kangaroo));
        return view("kangaroo.edit", ["kangaroo" => $kangaroo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KangarooRequest $request, $id)
    {
        $response = $this->service->store($request, $id);
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $response = $this->service->delete($id);
        return response()->json($response, 200);
    }
}
