<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KangarooResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name . " 8888 " ,
            "nickname" => $this->nickname,
            "gender" => $this->gender,
            "color" => $this->color,
            "birthday" => $this->birthday,
            "friendliness" => $this->friendliness,
            "photo" => $this->photo ? config("app.url")."/".$this->photo : null,
            "created_at"  => date_format($this->created_at,"Y-M-d H:i:s a"),
            "updated_at"  => date_format($this->updated_at,"Y-M-d H:i:s a"),
        ];
    }
}
