<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Foundation\Http\FormRequest;

trait crud 
{
    //get the list of records with pagination
    public function getAll($request)
    {
        // $paginate = $request->paginate ?? 10;
        // $orderBy  = $request->orderBy ?? "created_at";

        // $lookUp   = $request->search ?? "";
        // $result   = $this->model
        //             ->when($request->has("search"), function($query) use ($lookUp) {
        //                 $query->whereLike($this->searchableColumns, $lookUp);
        //             })
        //             ->when(is_array($orderBy), function($query) use ($orderBy) {
        //                     foreach($orderBy as $orderField) {
        //                         $query->orderBy($orderField ,"asc");
        //                     }
        //                 })
        //             ->when(!is_array($orderBy), function($query) use ($orderBy) {
        //                 $query->orderBy($orderBy, "asc");
        //             })
        //             ->paginate($paginate);
        $result   = $this->model->all();
        return $this->modelResource::collection($result);
    }
    

    //get the list of records
    public function get()
    {
        $result   = $this->model->all();
        return $this->modelResource::collection($result);
    }


    //find using the incremental id of the table
    public function find(int $id)
    {
        $data = $this->model->findOrFail($id);
        return new $this->modelResource($data);
    }



    //find using the field and value pass to this function
    public function getBy($column, $value)
    {
        $data = $this->model->where($column, $value)->first();
        return new $this->modelResource($data);
    }


    
    //loop through the fillable columns and match on the payload to insert record on the table
    public function store($request, $id = null)
    {
        if ($id !== null) {
            $this->model = $this->find($id);
        }

        $uuid = Str::uuid(30);
        $columns = $this->model->getFillable();
        $fileColumns = $this->model->fileColumns;

        
        foreach($columns as $column) {
            if ($fileColumns && in_array($column, $fileColumns)) {
                Log::info("may file");
                if($request->file($column) !== null) {
                    $this->model[$column] = $this->storeFile($request, $column, $uuid);
                }
            }else {
                $this->model[$column] = $request[$column] ?? $this->model[$column];
            }
        }

        $this->model->save();
        $data = $this->model;
        
        if ($id != null) {
            $data = $this->model->find($id);
        }
        return new $this->modelResource($data);
    }


    //function to get the file attached from the payload and store on the storage folder
    public function storeFile($request, $columnName, $filename)
    {
        if ($request->hasFile($columnName)) {
            

            $file = $request->file($columnName);
            $name = $filename.".".$request->file($columnName)->getClientOriginalExtension();

            if(substr($file->getMimeType(), 0, 5) == 'image') {
                //need to save to table - need to code
                $filePath = storage_path('app/'.$this->fileStoragePath."/thumbnail");
                if(!File::exists($filePath)) {
                    File::makeDirectory($filePath, 0777, true, true);
                }

                $img = Image::make($file->path());
                $img->resize(200, 200, function ($const) {
                    $const->aspectRatio();
                })->save($filePath.'/'.$name);
            }
            
            $path = $request->file($columnName)->storeAs($this->fileStoragePath, $name);
            return str_replace("public","storage", $path);
        }
        return null;
    }


    //pass the id od the record to be delete
    public function delete($id)
    {
        $record = $this->find($id);
         if($record){
             $record->delete();
             return array(
                 "success" => true,
                 "message" => "Record deleted"
             );
         }
    }


}