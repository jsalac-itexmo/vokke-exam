<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kangaroo extends Model
{
    use HasFactory;

    protected $fillable = [ 
        "name",
        "nickname",
        "weight",
        "photo",
        "height",
        "gender",
        "color",
        "friendliness",
        "birthday",
    ];

    public $fileColumns = ["photo"];
}
