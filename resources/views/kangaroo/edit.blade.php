@extends("layouts.app")
@section("content")
    <div class="h-full flex flex-col max-w-6xl mx-auto bg-gray-200 h-screen">
        <form id="editForm" class=" h-full flex flex-col justify-center  items-center">
            <div class="flex flex-col w-1/2  items-center border border-gray-400 shadow-lg p-8 space-y-4">
                <h1 class="font-bold text-3xl text-gray-700">Kangaroo</h1>
                <input type="hidden" name="_method" value="patch" />
                <input type="hidden" id="recordId" name="id" value={{$kangaroo["id"]}} />
                <div class="flex flex-col w-full ">
                    <label>Name</label>
                    <input type="text" name="name" value={{$kangaroo["name"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="name - it should be unique" required /> 
                </div>
                <div class="flex flex-col w-full ">
                    <label>Nick Name</label>
                    <input type="text" name="nickname" value={{$kangaroo["nickname"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="nickname" required /> 
                </div>
                <div class="flex flex-col w-full ">
                    <label>Color</label>
                    <input type="text" name="color" value={{$kangaroo["color"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="color" required /> 
                </div>
                <div class="flex flex-col w-full ">
                    <label>Weight</label>
                    <input type="number" name="weight" value={{$kangaroo["weight"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="weight in kilo" required /> 
                </div>
                <div class="flex flex-col w-full ">
                    <label>Height</label>
                    <input type="number" name="height" value={{$kangaroo["height"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="height in cm" required /> 
                </div>
                <div class="flex flex-col w-full ">
                    <label>Birthday</label>
                    <input type="date"  name="birthday" value={{$kangaroo["birthday"]}}  class="text-box rounded-lg border-gray-300 px-2 py-1 shadow-lg border" placeholder="birth date" required /> 
                </div>

                <div class="flex flex-col w-full ">
                    <label>Gender</label>
                    <select name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    <select>
                </div>

                <div class="flex flex-col w-full ">
                    <label>Friendliness</label>
                    <select name="friendliness">
                        <option value="friendly">Friendly</option>
                        <option value="not friendly">Not Friendly</option>
                    <select>
                </div>

                <div class="flex flex-col w-full ">
                    <label>Photo</label>
                    <input type="file"  name="photo" value={{$kangaroo["photo"]}}   /> 
                </div>

                <div class="w-1/2 rounded-xl">
                    <img src="{{config('app.url')}}/{{$kangaroo["photo"]}}" alt="photo" />
                </div>
                
            </div>
            <div class="w-1/2 flex mt-2 justify-end space-x-4 ">
                <button type="submit" class="border px-2 py-1 bg-blue-300 rounded-lg">Update </button>
                <button type="button" id="cancelUpdate" class="border px-2 py-1 bg-orange-300 rounded-lg">Cancel </button>
            </div>
           

        </form>
    </div>
@endsection

@section("script")
    <script>
          $(document).ready(function(){

            var appUrl = '{{ env('APP_URL') }}';


            $("#cancelUpdate").click(function() {
                window.location.href="/kangaroos"
            });

            $("form#editForm").on("submit", function(e) {
                e.preventDefault();    
                
                let recordId = $("#recordId").val();
                var formData = new FormData(this);
  
                let url = appUrl + "/api/kangaroos/" + recordId

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    success: function (data) {
                        window.location.href="/"
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            });
          });
    </script>
@endsection