@extends("layouts.app")
@section("content")
    <div class="flex flex-col max-w-6xl mx-auto bg-gray-200 h-screen space-y-4">
        <div class="flex justify-center"> 
            <h1 class="text-3xl font-bold text-gray-800 text-4xl" >{{config('app.name')}}</h1>
        </div>  
        <div class="flex justify-between px-4"> 
            <h1 class="text-2xl font-semibold">List of Kangaroos</h1>
            <button type="button" id="createNew"  class="flex  bg-blue-400 rounded-lg px-3 py-2">Add new</button>
        </div>
        <div  class="dx-viewport px-4 bg-gray-200">
           <div id="dataGrid" >
           </div>
        </div>
    </div>
@endsection

@section("script")
    <script>

        $(document).ready(function(){

            var appUrl = '{{ env('APP_URL') }}';

            $("#createNew").click(function() {
                window.location.href="/kangaroos/create"
            });

            $(function() {
                fetchData();
            });

            function fetchData() {
                var list = [];
                $.ajax({url: `${appUrl}/api/kangaroos`, success: function(result){
                    list = result.data
                    $("#dataGrid").dxDataGrid({
                        dataSource: list,
                        filterRow: { visible: true },
                        searchPanel: { visible: true },
                        selection: { mode: "single" },
                        columns: [{
                            dataField: "name"
                            }, {
                                dataField: "nickname"
                            }, {
                                dataField: "birthday", 
                                dataType: "date",
                            }, {
                                dataField: "gender"
                            }, {
                                dataField: "color"
                            }, {
                                dataField: "friendliness"
                            }, {
                                name: "edit",
                                type: 'buttons',
                                width: 110,
                                buttons: ['edit' ,'delete', {
                                    icon: 'edit',
                                    hint: "Cilck to edit",
                                    onClick(e) {
                                        let record = e.row.data;
                                        console.log(record);
                                        window.location.href="/kangaroos/"+record.id
                                    },
                                }],
                            }, {
                                name: "delete",
                                type: 'buttons',
                                width: 110,
                                buttons: ['delete', {
                                    icon: 'remove',
                                    hint: "Cilck to remove",
                                    onClick(e) {
                                        let record = e.row.data;
                                        Swal.fire({
                                            title: 'Do you want to delete this record?',
                                            showCancelButton: true,
                                            confirmButtonText: 'Delete',
                                            }).then((result) => {
                                            /* Read more about isConfirmed, isDenied below */
                                            if (result.isConfirmed) {
                                                $.ajax({url: `${appUrl}/api/kangaroos/${record.id}`, 
                                                    method:"delete",
                                                    success: function(result){
                                                        console.log("result", result);
                                                        fetchData();
                                                }});
                                            } 
                                        })
                                    },
                                }],
                            }],
                        allowColumnReordering: true,
                    });

                }});
            }
        });
    </script>
@endsection