<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kangaroos', function (Blueprint $table) {
            $table->id();
            $table->string("name", 85)->unique();
            $table->string("nickname", 50)->nullable();
            $table->string("photo")->nullable();
            $table->float("weight", 8,2)->default(0);
            $table->float("height", 8,2)->default(0);
            $table->string("gender", 20)->nullable();
            $table->string("color")->nullable();
            $table->string("friendliness", 20)->nullable();
            $table->date("birthday")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kangaroos');
    }
};
